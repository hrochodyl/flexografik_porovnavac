#include <Keypad.h>
#include <LiquidCrystal.h>

const float verzia = 1.01;

// enkoder na valci
struct Enkoder {
  byte pinA = 2;
  byte pinB = 4;  
  long hodnota = 0;
  bool stav = false; // zapnuty = true; vypnuty = false;
  short rozlisenie = 128; // [ks] Pocet impulzov na otacku
  float obvod = 0.324; // [m] Obvod valca
  bool smer = true; // posledny stav otacania valca; dopredu = true, dozadu = false
} valec;

// fotohlava
struct Fotohlava {
  byte pin = 3;
  long hodnota = 0;
  bool stav = false; // zapnuta = true; vypnuta = false;
  long valec_hodnota = 0; // hodnota valca v stave ked prisiel impulz na fotohlavu
  float tolerancia = 0.005; // [m] kolko metrov pred koncom zapocitaneho raportu ma fotohlava zacat snimat dalsiu znacku
} fotohlava;

const byte ALARM_PIN = 12; // pin pre alarm
const byte STOP_PIN = 13; // pin pre zastavenie stroja

float valec_krok = (valec.obvod) / valec.rozlisenie; // prepocet mm na m a nasledne delenie

// klavesnicka
const byte kRiadky = 4;
const byte kStlpce = 4;
char keys[kRiadky][kStlpce] = {
  {'1', '2', '3', 'A'},
  {'4', '5', '6', 'B'},
  {'7', '8', '9', 'C'},
  {'*', '0', '#', 'D'}
};
byte riadkyPin[kRiadky] = {0, 1, A5, A4}; // riadky
byte colPins[kStlpce] = {A3, A2, A1, A0}; // stlpce
Keypad keypad = Keypad( makeKeymap(keys), riadkyPin, colPins, kRiadky, kStlpce );

// displej
const byte DISPLEJ_ZNAKOV = 20;
const byte DISPLEJ_RIADKOV = 4;
LiquidCrystal lcd(10, 9, 5, 6, 7, 8); // RS, Enable, D4, D5, D6, D7
byte p20[8] = {
  B10000,
  B10000,
  B10000,
  B10000,
  B10000,
  B10000,
  B10000,
  B10000,  
};
byte p40[8] = {
  B11000,
  B11000,
  B11000,
  B11000,
  B11000,
  B11000,
  B11000,
  B11000,
};
byte p60[8] = {
  B11100,
  B11100,
  B11100,
  B11100,
  B11100,
  B11100,
  B11100,
  B11100,
};
byte p80[8] = {
  B11110,
  B11110,
  B11110,
  B11110,
  B11110,
  B11110,
  B11110,
  B11110,  
};
byte ph20[8] = {
  B10000,
  B10100,
  B00101,
  B11110,
  B00101,
  B10100,
  B10000,
  B10000,  
};
byte ph40[8] = {
  B11000,
  B11100,
  B01101,
  B10110,
  B01101,
  B11100,
  B11000,
  B11000,  
};
byte ph60[8] = {
  B11100,
  B11000,
  B01001,
  B10010,
  B01001,
  B11000,
  B11100,
  B11100,
};
byte ph80[8] = {
  B11110,
  B11010,
  B01011,
  B10000,
  B01011,
  B11010,
  B11110,
  B11110,
};
byte ph100[8] = {
  B11111,
  B11011,
  B01010,
  B10001,
  B01010,
  B11011,
  B11111,
  B11111,
};
byte sipkaVpravo[8] = {
  B11111,
  B11011,
  B11101,
  B00000,
  B11101,
  B11011,
  B11111,
};

// menu
const byte MENU_1 = 1;
const byte MENU_2 = 2;
byte aktivneMenu = MENU_1; // menu nastavenia hodnot
byte aktivnaPolozkaMenu = 0;
const int MENU_ZOBRAZOVACI_INTERVAL = 50; // menu sa zobrazi kazdych x ms
long menu_posledne_zobrazenie;

const float RAPORT_MIN = 0.01;
const float RAPORT_MAX = 999.9;
const float OBRAZKY_MIN = 0.01;
const long OBRAZKY_MAX = 99999;
const byte METRE_MIN = 1;
const int METRE_MAX = 9999;
const byte SKLZ_NORMAL = 3;
const byte SKLZ_MIN = 1;
const byte SKLZ_MAX = 99;
const byte ALARM_PERCENTA_NORMAL = 90;
const byte ALARM_PERCENTA_MIN = 1;
const byte ALARM_PERCENTA_MAX = 99;

struct Data {
  float raport;
  long obrazky; // pocet obrazkov podla metrov
  float metre;
  float sklz; // metre < fotohlava: kladny; metre > fotohlava: zaporny
  int alarm;
} nastavenia, uloha;

// mod aplikacie
const byte MOD_CHYBA = -1;
const byte MOD_METRE = 1;
const byte MOD_OBRAZKY = 2;
const byte MOD_ETIKETY = 3;

// typy cisel pre vstup z klavesnice
const byte FORMAT_CELE = 1;
const byte FORMAT_DESATINNE = 2;

unsigned long currentMillis;

/*
 * SETUP 
 *
 */
void setup() {
  delay(300);

  // inicializacia lcd
  lcd.createChar(0, p20);
  lcd.createChar(1, p40);
  lcd.createChar(2, p60);
  lcd.createChar(3, p80); 
  lcd.createChar(4, ph20);
  lcd.createChar(5, ph60);
  lcd.createChar(6, ph100);
  lcd.createChar(7, sipkaVpravo);

  delay(300); 
  lcd.begin(DISPLEJ_ZNAKOV, DISPLEJ_RIADKOV);
  delay(150); 
  
  // inicializacia enkoder
  pinMode(valec.pinA, INPUT);
  digitalWrite(valec.pinA, HIGH);
  pinMode(valec.pinB, INPUT);
  digitalWrite(valec.pinB, HIGH);
  attachInterrupt(0, INTPosunValca, RISING);
  
  // inicializacia fotohlava
  pinMode(fotohlava.pin, INPUT);
  digitalWrite(fotohlava.pin, HIGH);
  attachInterrupt(1, INTFotohlava, RISING);
  
  // inicializacia vystupov
  pinMode(ALARM_PIN, OUTPUT);
  pinMode(STOP_PIN, OUTPUT);
  
  // predvolene hodnoty nastaveni
  nastavenia.sklz = SKLZ_NORMAL;
  nastavenia.alarm = ALARM_PERCENTA_NORMAL;
  
  ResetUlohy();
  
  UkazIntro();
    
//  Serial.begin(9600);
}

/*
 * Reset vsetkych udajov ulohy
 */
void ResetUlohy() {
  valec.hodnota = 0;
  valec.stav = false;
  valec.smer = true;
  fotohlava.hodnota = 0;
  fotohlava.valec_hodnota = 0;  
  fotohlava.stav = false;
  
  uloha.obrazky = 0;
  uloha.metre = 0;
  uloha.sklz = 0;
  
  digitalWrite(ALARM_PIN, LOW);
  digitalWrite(STOP_PIN, LOW);
}

/*
 * LOOP
 *
 */
void loop() {
  
  currentMillis = millis();
  
  uloha.metre = valec_krok * valec.hodnota;
  
  if (modAplikacie() == MOD_OBRAZKY) { // mode pocitania obrazkov    
    if (fotohlava.hodnota > 0) { // zaciname pocitat az pokial nepride 1vy obrazok z fotohlavy
      if (valec.stav == false && fotohlava.hodnota == 1) { // fotohlava zaznamenala prvy impulz, co ale neznamena ze to uz je prvy obrazok. Takze ten odpocitame lebo este len pride.
        fotohlava.hodnota = 0;
      }
//      if(valec.stav == false) {
//        valec.stav += valec.rozlisenie;
//      }
      valec.stav = true;
            
      // pocitanie sklzu
      uloha.obrazky = uloha.metre / nastavenia.raport;
      if(uloha.obrazky < fotohlava.hodnota) {
        uloha.sklz = ((uloha.obrazky - fotohlava.hodnota) * 100) / (float)fotohlava.hodnota;  
      } else if(uloha.obrazky > fotohlava.hodnota) {
        uloha.sklz = ((uloha.obrazky - fotohlava.hodnota) * 100) / (float)uloha.obrazky;  
      } else {
        uloha.sklz = 0.0;
      }
     
      // upozornenie pri sklze 
      if(nastavenia.sklz > SKLZ_MIN) {
        if (abs(uloha.sklz) > nastavenia.sklz) {
          digitalWrite(ALARM_PIN, HIGH);
        } else {
          digitalWrite(ALARM_PIN, LOW);
        }
      }
      
      kontrolaZastavenia();
      
    } else {
      fotohlava.stav = true;
    }
      
  } else if (modAplikacie() == MOD_ETIKETY) {
    
    fotohlava.stav = true;
    valec.stav = true;
    
    kontrolaZastavenia();
    
  } else if (modAplikacie() == MOD_METRE) { // mode pocitanie metrov
    
    valec.stav = true;
    
    kontrolaZastavenia();
    
  } 
  
    
  if(currentMillis - menu_posledne_zobrazenie > MENU_ZOBRAZOVACI_INTERVAL) {
    menu_posledne_zobrazenie = millis();
    Menu();
  }
  
}


void Menu() {
  
  char klaves = keypad.getKey();
  
  if (aktivneMenu == MENU_1) { // HLAVNE MENU
    if(modAplikacie() != MOD_CHYBA) {
      
      byte x = 0;
      byte y = 0;
    
      if(modAplikacie() == MOD_OBRAZKY || modAplikacie() == MOD_ETIKETY) {
        lcd.setCursor(x, y++);
        lcd.print("Obrazky "); lcd.print(fotohlava.hodnota); lcd.print("ks ");
      } else if(modAplikacie() == MOD_OBRAZKY || modAplikacie() == MOD_ETIKETY) {
        lcd.setCursor(x, y++);
        lcd.print("Obrazky "); lcd.print(uloha.obrazky); lcd.print("ks ");
      }
      
      lcd.setCursor(x, y++);
      lcd.print("Metre "); lcd.print(uloha.metre); lcd.print("m ");        

      if(modAplikacie() == MOD_OBRAZKY) {
        lcd.setCursor(x, y++);          
        lcd.print("Sklz "); lcd.print(uloha.obrazky - fotohlava.hodnota); lcd.print("ks ");
        lcd.print((int) uloha.sklz); lcd.print("%  ");
      }

      // vykreslovanie progress baru
      switch (modAplikacie()) {  
        case MOD_OBRAZKY:
        case MOD_ETIKETY:
          Graf(0, 3, min(fotohlava.hodnota / (float) nastavenia.obrazky * 100, 100.0));
          break;
          
        case MOD_METRE:
          Graf(0, 3, min(uloha.metre / nastavenia.metre * 100, 100.0));        
          break;
      }
      
      if (klaves != NO_KEY){
        lcd.clear();
      }
      if (klaves == 'A') {
        if(aktivnaPolozkaMenu == 0) {
          aktivnaPolozkaMenu = 0;
        } else {
          aktivnaPolozkaMenu--;
        }
      } else if (klaves == 'B') {
        if(aktivnaPolozkaMenu == 0) {
          aktivnaPolozkaMenu = 0;  
        } else {
          aktivnaPolozkaMenu++;
        }
      } else if (klaves == 'C') {
        ResetUlohy();
        UkazSpravu("Reset hodnot OK", 3000);
      }
      
    } else { // MOD CHYBY
      lcd.setCursor(0, 1);
      lcd.print("Nastav hodnoty.");
      lcd.setCursor(0, 2);
      lcd.print("Stlac *");    
    }
   
    if (klaves == '*') {
      aktivnaPolozkaMenu = 0;
      aktivneMenu = MENU_2;
      lcd.clear();
    }
    
  } else if (aktivneMenu == MENU_2) { // MENU NASTAVENIA
  
    // vykreslenie znakov
    switch (aktivnaPolozkaMenu) {
      
      case 0: case 1: case 2: case 3:
        lcd.setCursor(1, 0);
        lcd.print("Raport ");
        lcd.print(nastavenia.raport * 1000);
        lcd.print("mm");

        lcd.setCursor(1, 1);
        lcd.print("Obrazkov ");
        lcd.print(nastavenia.obrazky);
        lcd.print("ks");
        
        lcd.setCursor(1, 2);
        lcd.print("Metrov ");
        lcd.print(nastavenia.metre);
        lcd.print("m");

        lcd.setCursor(1, 3);
        lcd.print("Sklz ");
        if(nastavenia.sklz > SKLZ_MIN) {
          lcd.print(nastavenia.sklz);
          lcd.print("%");
        } else {
          lcd.print("Vyp");
        }
        break;
        
      case 4:
        lcd.setCursor(1, 0);
        lcd.print("Upozornenie ");
        lcd.setCursor(13, 0);
        if(nastavenia.alarm > ALARM_PERCENTA_MIN) {
          lcd.print(nastavenia.alarm);
          lcd.print("%");
        } else {
          lcd.print("Vyp");
        }
    }
    
    // obsluha tlacidiel
    switch (aktivnaPolozkaMenu) {
      case 0:
        lcd.setCursor(0, 0); lcd.write((byte)126);
        lcd.setCursor(0, 1); lcd.print(" ");
        lcd.setCursor(0, 2); lcd.print(" ");
        lcd.setCursor(0, 3); lcd.print(" ");
      
        if (klaves == 'D') {
          lcd.setCursor(0, 0); lcd.write((byte)7);
          
          nastavenia.raport = NacitajCislo(FORMAT_DESATINNE, RAPORT_MIN, RAPORT_MAX, true, 8, 0);
          nastavenia.raport *= 0.001;
          
          if(nastavenia.raport > RAPORT_MIN && nastavenia.obrazky > OBRAZKY_MIN) {
            nastavenia.metre = PrepocitajMetre(nastavenia.raport, nastavenia.obrazky);
          }
        } else if (klaves == 'C') {
          nastavenia.raport = 0;
        }
        break;

      case 1:
        lcd.setCursor(0, 0); lcd.print(" ");
        lcd.setCursor(0, 1); lcd.write((byte)126);
        lcd.setCursor(0, 2); lcd.print(" ");
        lcd.setCursor(0, 3); lcd.print(" ");
        
        if (klaves == 'D') {
          lcd.setCursor(0, 1); lcd.write((byte)7);
          
          nastavenia.obrazky = NacitajCislo(FORMAT_CELE, OBRAZKY_MIN, OBRAZKY_MAX, true, 10, 1);
          
          if(nastavenia.raport > RAPORT_MIN && nastavenia.obrazky > OBRAZKY_MIN) {
            nastavenia.metre = PrepocitajMetre(nastavenia.raport, nastavenia.obrazky);
          }
        } else if (klaves == 'C') {
          nastavenia.obrazky = 0;
        }
        break;

      case 2:
        lcd.setCursor(0, 0); lcd.print(" ");
        lcd.setCursor(0, 1); lcd.print(" ");
        lcd.setCursor(0, 2); lcd.write((byte)126);
        lcd.setCursor(0, 3); lcd.print(" ");
        
        if (klaves == 'D') {
          lcd.setCursor(0, 2); lcd.write((byte)7);
          
          nastavenia.metre = NacitajCislo(FORMAT_CELE, METRE_MIN, METRE_MAX, true, 8, 2);
          
          if(nastavenia.metre > METRE_MIN && nastavenia.metre < METRE_MAX) {
            nastavenia.raport = 0;
            nastavenia.obrazky = 0;
          }
        } else if (klaves == 'C') {
          nastavenia.metre = 0;
          nastavenia.raport = 0;
          nastavenia.obrazky = 0;
        }
        break;

      case 3:    
        lcd.setCursor(0, 0); lcd.print(" ");
        lcd.setCursor(0, 1); lcd.print(" ");
        lcd.setCursor(0, 2); lcd.print(" ");
        lcd.setCursor(0, 3); lcd.write((byte)126);
        
        if (klaves == 'D') {
          lcd.setCursor(0, 3); lcd.write((byte)7);
          
          nastavenia.sklz = NacitajCislo(FORMAT_CELE, SKLZ_MIN, SKLZ_MAX , true, 6, 3);
          
        } else if (klaves == 'C') {
          nastavenia.sklz = 0;
        }
        break;
        
      case 4:
        lcd.setCursor(0, 0); lcd.write((byte)126);
        lcd.setCursor(0, 1); lcd.print(" ");
        
        if (klaves == 'D') {
          lcd.setCursor(0, 0);
          lcd.write((byte)7);
          nastavenia.alarm = NacitajCislo(FORMAT_CELE, ALARM_PERCENTA_MIN, ALARM_PERCENTA_MAX , true, 13, 0);
        }
        if (klaves == 'C') {
          nastavenia.alarm = 0;
        }
        break;        
        
    }

    if (klaves != NO_KEY){
      lcd.clear();
    }
    if (klaves == 'A') {
      if(aktivnaPolozkaMenu == 0) {
        aktivnaPolozkaMenu = 4;
      } else {
        aktivnaPolozkaMenu--; 
      }
    } else if (klaves == 'B') {
      if(aktivnaPolozkaMenu == 4) {
        aktivnaPolozkaMenu = 0;  
      } else {
        aktivnaPolozkaMenu++;
      }
    } else if (klaves == '*') {
      aktivnaPolozkaMenu = 0;
      aktivneMenu = MENU_1;
      lcd.clear();
    }
  }

}

/*
 * Vykresluje progress bar
 *
 * x - suradnica x na displeji
 * y - suradnica y na displeji
 * percent - hodnota percent
 */
void Graf(byte x, byte y, float percent) {
  
  lcd.setCursor(x, y);
  float hodnota = (float) DISPLEJ_ZNAKOV / 100 * percent;
  int alarm_pozicia = (float) DISPLEJ_ZNAKOV / 100 * nastavenia.alarm;
  
  for (int i = 0; i < DISPLEJ_ZNAKOV; i++) {
    if(hodnota > i) {
      if((hodnota - i) < 1) {
        
        if(i != alarm_pozicia) {
          switch ((int) (5 * (hodnota - i))) {
            case 0:
              lcd.print(" ");
              break;
            
            case 1:
              lcd.write((byte)0);
              break;
          
            case 2:
              lcd.write((byte)1);
              break;
          
            case 3:
              lcd.write((byte)2);
              break;
          
            case 4:
              lcd.write((byte)3);
              break;
              
            case 5:
              lcd.write((byte)255);
              break;
              
          }
        } else {
          switch ((int) (5 * (hodnota - i))) {
            case 0:
            case 1:
              lcd.write((byte)4);
              break;
          
            case 2:          
            case 3:
              lcd.write((byte)5);
              break;
          
            case 4:
            case 5:
              lcd.write((byte)6);
              break;
          }
        }
      } else {
        if(i == alarm_pozicia) {
          lcd.write((byte)6);
        } else {
          lcd.write((byte)255);   
        }
      }    
    } else {
      if(i == alarm_pozicia) {
        lcd.print("*");
      } else {
        lcd.print(" ");
      }
    }
  } 
}

/*
 * Urcuje aky je aktualny mod aplikacie
 *
 * vracia mod aplikacie MOD_OBRAZKY, MOD_METRE; 
 */
byte modAplikacie() {
  
  if (nastavenia.obrazky > OBRAZKY_MIN && nastavenia.raport > RAPORT_MIN && nastavenia.metre > METRE_MIN) { // mode pocitania obrazkov
    return MOD_OBRAZKY;
  } else if (nastavenia.metre > METRE_MIN && nastavenia.raport < RAPORT_MIN && nastavenia.obrazky < OBRAZKY_MIN) {
    return MOD_METRE;
  } else if (nastavenia.obrazky > OBRAZKY_MIN && nastavenia.metre < METRE_MIN && nastavenia.raport < RAPORT_MIN) {
    return MOD_ETIKETY;  
  }
    
  return MOD_CHYBA;
}

/*
 * Nacitava cislo z klavesnice
 *
 * vypis - budeme vypisovat na obrazovku?'
 * format - FORMAT_CELE, FORMAT_DESATINNE
 * minimum
 * maximum
 * vypis - ci sa ma vypisovat alebo nie
 * x - suradnica x na displeji
 * y - suradnica y na displeji
 */
float NacitajCislo(int format, float minimum, float maximum, bool vypis, byte x, byte y)
{
  float num = 0;
  float newNum = 0;
  float desatiny = 1;
  bool realMode = true;

  byte celych_miest = 0;
  byte desatinnych_miest = 0;
  

  char klaves = keypad.getKey();
  
  if (vypis) {
     // zmazanie do konca riadka
    for(int i=x; i<DISPLEJ_ZNAKOV; i++) {
      lcd.setCursor(i, y);
      lcd.print(" ");        
    }
    lcd.setCursor(x,y);
    lcd.blink();
  }
  
  while (klaves != 'D')
  {
    switch (klaves)
    {
      case NO_KEY:
        break;

      case '0': case '1': case '2': case '3': case '4':
      case '5': case '6': case '7': case '8': case '9':
        newNum = num * 10 + (klaves - '0');

        if (realMode) {
          celych_miest++;
        } else {
          desatinnych_miest++;
          desatiny = desatiny * 0.1;
        }

        if ((newNum * desatiny) > maximum) {
          if(vypis) {
            lcd.noBlink();
          }
          num = 0;
          lcd.noBlink();
          UkazSpravu("Mimo rozsah!", 3000);
          return num;
        } else {
          num = newNum;
        }
        break;

      case '#':
        if (realMode && (format == FORMAT_DESATINNE)) {
          realMode = false;
        }
        break;

      case '*': case 'C':
        if(vypis) {
          lcd.noBlink();
        }
        num = 0;
        return num;
        break;
    }
    
    // vypis cisla na displej
    if (vypis && klaves != NO_KEY) { 
      lcd.setCursor(x, y);

      switch (format)
      {
        case FORMAT_DESATINNE:
          lcd.print(num * desatiny);
          if(realMode) {
            lcd.setCursor(x+celych_miest, y);
          } else {
            lcd.setCursor(x+celych_miest+desatinnych_miest+1, y);
          }
          break;
        case FORMAT_CELE:
          lcd.print((int)num);
      }
    }    

    klaves = keypad.getKey();
  }

  if ((num * desatiny) < minimum) {
    lcd.noBlink();
    UkazSpravu("Mimo rozsah!", 3000);
    return 0;
  }

  lcd.noBlink();
  return num * desatiny;
}

/*
 * Ukazanie spravy na urcity casovy interval
 *
 * sprava - sprava na vypis
 * trvanie - dlzka spravy v ms
 */
void UkazSpravu(String sprava, int trvanie) {
  lcd.clear();
  lcd.setCursor(0,1);
  lcd.print(sprava);
  delay(trvanie);
  lcd.clear();
}

void UkazIntro() {
  lcd.clear();
  lcd.setCursor(0,1);
  lcd.print("Citac, verzia. ");lcd.print(verzia);
  lcd.setCursor(3,3);
  lcd.print("Bartos & syn 2015");
  delay(4000);
  lcd.clear();
}

float PrepocitajMetre(float raport, int obrazkov) {
  return raport * obrazkov;
}

/*
 * Stopne zariadenie pri dosiahnuti konca ulohy
 *
 */
void kontrolaZastavenia() {
  if(nastavenia.alarm > ALARM_PERCENTA_MIN) {
    switch (modAplikacie()) {
      case MOD_OBRAZKY:
      case MOD_ETIKETY:
        if (fotohlava.hodnota >= nastavenia.obrazky) {
          digitalWrite(STOP_PIN, HIGH);        
        }
        break;
           
      case MOD_METRE:
        if(uloha.metre >= nastavenia.metre) {
          digitalWrite(STOP_PIN, HIGH);
        }
        break;
    }  
  }
}

/*
 * Funkcia volana enkoderom na valci
 */
void INTPosunValca() {
  if(valec.stav && modAplikacie() != MOD_CHYBA) {
    if (digitalRead(valec.pinA) == digitalRead(valec.pinB)) {
      valec.hodnota++;
      valec.smer = true;
    } else {
      valec.hodnota--;
      valec.smer = false;
    }
  }
  
  switch (modAplikacie()) {  
    
    case MOD_OBRAZKY:
      if (fotohlava.hodnota > 0) {
        float metre_rozdiel_od_fotohlavy = valec_krok * (valec.hodnota - fotohlava.valec_hodnota);
        if(metre_rozdiel_od_fotohlavy < (nastavenia.raport - fotohlava.tolerancia)) {
          fotohlava.stav = false;
        } else {
          fotohlava.stav = true;
        }
      }   
      break; 
      
    case MOD_METRE:
    case MOD_ETIKETY:
      break;
  }
}

/*
 * Funkcia volana fotohlavou pri znacke
 */
void INTFotohlava() {
  if(fotohlava.stav && modAplikacie() != MOD_CHYBA) {
    if(valec.smer) {    
      fotohlava.hodnota++;
    } else {
      fotohlava.hodnota--;
    }
  }
  
  switch (modAplikacie()) {  
    case MOD_OBRAZKY:
      if(fotohlava.stav) {
        fotohlava.valec_hodnota = valec.hodnota;
        fotohlava.stav = false;
      }
      break; 
      
    case MOD_METRE:
    case MOD_ETIKETY:
      break;
  }
}
